Npm 
=========

This role installs:
- `npm`
- increase `sysctl` notify watches
- `n` npm packages globally
- `nodejs` via `n` npm packages 


Role Variables
--------------

|name|description|
|---|---|
|sysctl_max_notify_watcher|The amount of watchers for sysctl notify watchers. It's needed for Angular Live Reload|

